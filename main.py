import random

from sayer import say

if __name__ == '__main__':
    score = 0

    while True:
        max_range = 10

        if score > 10:
            max_range = 100
        elif score > 5:
            max_range = 20

        number = random.randrange(1, max_range)

        user_input = None

        while str(number) != user_input:
            if user_input not in [None, ""]:
                say("Není to dobře, napsalas " + user_input)

            say("Napiš " + str(number))

            user_input = input("> ")

        score += 1
        say("Správně, máš bodů: " + str(score))
