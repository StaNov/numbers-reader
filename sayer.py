import tempfile

import gtts
import playsound


def say(text_to_say):
    tts = gtts.gTTS(text=text_to_say, lang="cs")
    with tempfile.NamedTemporaryFile() as f:
        tts.save(f.name)
        playsound.playsound(f.name)